public class Items {
    int itemId;
    String shortDescribtion;
    String brand;
    int price;


    public Items(int itemid, String shortDescribtion, String brand, int price) {
        this.itemId = itemid;
        this.shortDescribtion = shortDescribtion;
        this.brand= brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    public String toString() {
        return "Items[itemid=" + itemId + ",shortDescribtion=" + shortDescribtion + ",Brand=" + brand + ",Price=" + price + "]";
    }
}